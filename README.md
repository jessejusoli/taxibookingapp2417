# Taxi Booking App - A Complete Clone of UBER

Taxi Booking App - A Complete Clone of UBER without User App and Driver App.  Just Backend CMS (core, system).
Does not include the user application and driver application. Just Backend CMS (core, system).

*Taxi Booking App - A Complete Clone of UBER Backend CMS (System Core).*

---


## Features in CMS :

1. Clean and Easy to understand Dashboard which display all relevant statistics along with timelines for recent bookings .
2. Option to View all Booking with Search.
3. View all Driver’s .
4. View all Usre’s .
5. Assign Job to Driver from CMS.
6. View Flagged Driver and Option to block them.
7. View Flagged User and Option to block users.
8. Option to view all users.
9. Option to Set car types .
10. Set Prices for Car types .
11. Set Currencies .
12. Set Day and Night timings.  

# Installing Plugins :

With New version of android we are offering plugins as well. 


# Installation of Server side script :

--Server side requires Cloud  server , This script won't get hosted on any shared hosting . We need  Php7.0 , Mysql , Node.js and Mongodb as prerequisites to set up the script .  I will cover steps on how to install LAMP and MEAN stack on server and the steps to deploy this code to your server .


# Few things  which could help you :

A Cloud or VPS is the type of server where we get entire root access of the server . Now a day’s any cloud or shared hosting will almost cost you the same , indeed cloud hosting are cheaper compare to shared hosting now a day . You great great deal’s of cloud with 
Linode.com  , digitalocean.com  , https://contabo.com etc .  They are very cheaper . You can also try amazon ec2 server as they are good as well .

*We have installed this app on Ubuntu 16.0.4 but you can try centos ( we don't have tried installing on centos  but many of our client did the same and they were able to get this working. )*

#Installation of Server side script :

1.	Upload entire folder content of “CMS & Database” into your public html folder ( exclude db.sql file )
2.	Then goto application folder -> config -> database.php and write database credentials according to following instructions.

$db["default"]["hostname"] = "YOUR HOST NAME";

$db["default"]["username"] = "DATABASE USERNAME";

$db["default"]["password"] = "DATABASE PASSWORD";

$db["default"]["database"] ="DATABASE NAME";


Access cms from http://ipaddress/cms
Access api from http://ipaddress/web_service


3. Now go to application folder -> controllers -> web_service.php and find “send_mail” function. In this function you need to change following lines according to instructions.
		
$mail->Username   = "RECEIVER GMAIL EMAIL ID";

$mail->Password   = "RECEIVER GMAIL ACCOUNT PASSWORD";

$mail->SetFrom('SENDER EMAIL ID', 'SENDER NAME');

$mail->AddReplyTo("REPLY EMAIL ID","REPLY NAME");


---

# consult file for build: /doc/build-unix.md
