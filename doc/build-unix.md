# Taxi Booking App - A Complete Clone of UBER - CMS (System core)

## Features in CMS :
* Clean and Easy to understand Dashboard which display all relevant statistics along with timelines for recent bookings .
* Option to View all Booking with Search.
* View all Driver’s .
* View all Usre’s .
* Assign Job to Driver from CMS.
* View Flagged Driver and Option to block them.
* View Flagged User and Option to block users.
* Option to view all users.
* Option to Set car types .
* Set Prices for Car types .
* Set Currencies .
* Set Day and Night timings.  

## Installing Plugins :

With New version of android we are offering plugins as well. 


## Installation of Server side script :

--Server side requires Cloud  server , This script won't get hosted on any shared hosting . We need  Php7.0 , Mysql , Node.js and Mongodb as prerequisites to set up the script .  I will cover steps on how to install LAMP and MEAN stack on server and the steps to deploy this code to your server .


## Few things  which could help you :

A Cloud or VPS is the type of server where we get entire root access of the server . Now a day’s any cloud or shared hosting will almost cost you the same , indeed cloud hosting are cheaper compare to shared hosting now a day . You great great deal’s of cloud with 
Linode.com  , digitalocean.com  , https://contabo.com etc .  They are very cheaper . You can also try amazon ec2 server as they are good as well .

We have installed this app on Ubuntu 16.0.4 but you can try centos ( we don't have tried installing on centos  but many of our client did the same and they were able to get this working. )


How to install LAMP on Cloud server 
LAMP stands for Linux , Apache , Mysql and PHP . We also need curl to be installed on this server 
Login to your cloud server using ssh using your terminal , if you are using windows you can download putty and login to console .

## Step 1: In first step we will install apache
```
sudo apt-get update
sudo apt-get install apache2 (This command will install apache on your server)
sudo apache2ctl configtest ( This check if apache is installed correctly )
```
Output
```
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerName' directive globally to suppress this message
Syntax OK
```

Now open apache2.conf  by typing below command 
```
sudo nano /etc/apache2/apache2.conf
```
Inside, at the bottom of the file, add a ServerName directive, pointing to your primary domain name. If you do not have a domain name associated with your server, you can use your server's public IP address:
```
ServerName <ip-address of your cloud server>
```
Next, check for syntax errors by typing:
```
sudo apache2ctl configtest 
```
```
(Output
Syntax OK
```
Now:
```
sudo systemctl restart apache2
```
Type this on your browser and you will see apache page http://your_server_IP_address
To install curl type this 2 command
```
sudo apt-get install curl
curl http://icanhazip.com
```
We will also install Php-Curl as that is required for Cron service
```
sudo apt-get install php-curl
```

## Step 2: Install MySQL
Now that we have our web server up and running, it is time to install MySQL. MySQL is a database management system. Basically, it will organize and provide access to databases where our site can store information.

Again, we can use apt to acquire and install our software. This time, we'll also install some other "helper" packages that will assist us in getting our components to communicate with each other:
```
sudo apt-get install mysql-server
```
Again, you will be shown a list of the packages that will be installed, along with the amount of disk space they'll take up. Enter Y to continue.

During the installation, your server will ask you to select and confirm a password for the MySQL "root" user. This is an administrative account in MySQL that has increased privileges. Think of it as being similar to the root account for the server itself (the one you are configuring now is a MySQL-specific account, however). Make sure this is a strong, unique password, and do not leave it blank.

When the installation is complete, we want to run a simple security script that will remove some dangerous defaults and lock down access to our database system a little bit. Start the interactive script by running:
```
mysql_secure_installation
```
You will be asked to enter the password you set for the MySQL root account. 
Next, you will be asked if you want to configure the VALIDATE PASSWORD PLUGIN.
Warning: Enabling this feature is something of a judgment call. If enabled, passwords which don't match the specified criteria will be rejected by MySQL with an error. This will cause issues if you use a weak password in conjunction with software which automatically configures MySQL user credentials, such as the Ubuntu packages for phpMyAdmin. It is safe to leave validation disabled, but you should always use strong, unique passwords for database credentials.
Answer y for yes, or anything else to continue without enabling.
VALIDATE PASSWORD PLUGIN can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD plugin?
```
Press y|Y for Yes, any other key for No:
```

You'll be asked to select a level of password validation. Keep in mind that if you enter 2, for the strongest level, you will receive errors when attempting to set any password which does not contain numbers, upper and lowercase letters, and special characters, or which is based on common dictionary words.
There are three levels of password validation policy:

LOW    Length >= 8
MEDIUM Length >= 8, numeric, mixed case, and special characters
STRONG Length >= 8, numeric, mixed case, special characters and dictionary                  file
```
Please enter 0 = LOW, 1 = MEDIUM and 2 = STRONG: 1
```

If you enabled password validation, you'll be shown a password strength for the existing root password, and asked you if you want to change that password. If you are happy with your current password, enter nfor "no" at the prompt:
Using existing password for root.
```
Estimated strength of the password: 100
Change the password for root ? ((Press y|Y for Yes, any other key for No) : n
```

For the rest of the questions, you should press Y and hit the Enter key at each prompt. This will remove some anonymous users and the test database, disable remote root logins, and load these new rules so that MySQL immediately respects the changes we have made.
At this point, your database system is now set up and we can move on.


## Step 3: Install PHP

PHP is the component of our setup that will process code to display dynamic content. It can run scripts, connect to our MySQL databases to get information, and hand the processed content over to our web server to display.

We can once again leverage the apt system to install our components. We're going to include some helper packages as well, so that PHP code can run under the Apache server and talk to our MySQL database:
```
sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql
```
This should install PHP without any problems. We'll test this in a moment.

In most cases, we'll want to modify the way that Apache serves files when a directory is requested. Currently, if a user requests a directory from the server, Apache will first look for a file called index.html. We want to tell our web server to prefer PHP files, so we'll make Apache look for an index.php file first.

To do this, type this command to open the dir.conf file in a text editor with root privileges:
```
sudo nano /etc/apache2/mods-enabled/dir.conf
```
It will look like this:
```
/etc/apache2/mods-enabled/dir.conf
<IfModule mod_dir.c>
    DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm
</IfModule>
```

We want to move the PHP index file highlighted above to the first position after the DirectoryIndex specification, like this:

/etc/apache2/mods-enabled/dir.conf
```
<IfModule mod_dir.c>
    DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm
</IfModule>
```
When you are finished, save and close the file by pressing Ctrl-X. You'll have to confirm the save by typing Y and then hit Enter to confirm the file save location.

After this, we need to restart the Apache web server in order for our changes to be recognized. You can do this by typing this:
```
sudo systemctl restart apache2
```
We can also check on the status of the apache2 service using systemctl:
```
sudo systemctl status apache2
```
Sample Output
```
● apache2.service - LSB: Apache2 web server
   Loaded: loaded (/etc/init.d/apache2; bad; vendor preset: enabled)
  Drop-In: /lib/systemd/system/apache2.service.d
           └─apache2-systemd.conf
   Active: active (running) since Wed 2016-04-13 14:28:43 EDT; 45s ago
     Docs: man:systemd-sysv-generator(8)
  Process: 13581 ExecStop=/etc/init.d/apache2 stop (code=exited, status=0/SUCCESS)
  Process: 13605 ExecStart=/etc/init.d/apache2 start (code=exited, status=0/SUCCESS)
    Tasks: 6 (limit: 512)
   CGroup: /system.slice/apache2.service
           ├─13623 /usr/sbin/apache2 -k start
           ├─13626 /usr/sbin/apache2 -k start
           ├─13627 /usr/sbin/apache2 -k start
           ├─13628 /usr/sbin/apache2 -k start
           ├─13629 /usr/sbin/apache2 -k start
           └─13630 /usr/sbin/apache2 -k start

Apr 13 14:28:42 ubuntu-16-lamp systemd[1]: Stopped LSB: Apache2 web server.
Apr 13 14:28:42 ubuntu-16-lamp systemd[1]: Starting LSB: Apache2 web server...
Apr 13 14:28:42 ubuntu-16-lamp apache2[13605]:  * Starting Apache httpd web server apache2
Apr 13 14:28:42 ubuntu-16-lamp apache2[13605]: AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 127.0.1.1. Set the 'ServerNam
Apr 13 14:28:43 ubuntu-16-lamp apache2[13605]:  *
Apr 13 14:28:43 ubuntu-16-lamp systemd[1]: Started LSB: Apache2 web server.
```
Type this command to install php-cli
```
sudo apt-get install php-cli
```
## Step 4: Test PHP Processing on your Web Server

In order to test that our system is configured properly for PHP, we can create a very basic PHP script.

We will call this script info.php. In order for Apache to find the file and serve it correctly, it must be saved to a very specific directory, which is called the "web root".

In Ubuntu 16.04, this directory is located at /var/www/html/. We can create the file at that location by typing:
```
sudo nano /var/www/html/info.php
```

This will open a blank file. We want to put the following text, which is valid PHP code, inside the file:

info.php
```
<?php
phpinfo();
?>
```

When you are finished, save and close the file.

Now we can test whether our web server can correctly display content generated by a PHP script. To try this out, we just have to visit this page in our web browser. You'll need your server's public IP address again.

The address you want to visit will be:
```
http://your_server_IP_address/info.php
```
You will find entire info page .


Now we have installed LAMP Stack successfully . Now connect to FTP and browse the folder  /var/www/html and inside this folder upload the code of “PHP-Code” . Dont upload this folder , just upload the content of folder .

Also make sure you have the .htaccess file copied as well . Since .htaccess file is always hidden and its not a part of download hence i am copying the content here . Just create a .htaccess file 
```

RewriteEngine on
RewriteCond $1 !^(index\.php|public|\.txt) 
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?$1

```
 	

Changes to made in PHP Code
```
(1)application/config/constants.php(Timezone,base url,base ip)
(2)application/config/database.php(host name,username,password,database name)
(3)application/controllers/web_service.php(name,email)
(4)application/controllers/admin.php(name,email)
(5)application/views/email_messages.php(email,email password,name)
(6)application/views/push_messages.php(name)
(7)includes/dbconfig.php(host name,username,password,database name)
(8)set permission(adminimage folder,car_image folder,driverimages folder,user_image folder)  ( sudo chmod 755 ) or you can set the permission using FTP - 755 .
```


How to Install  MEAN on Cloud server 


Ubuntu 16.04 contains a version of Node.js in its default repositories that can be used to easily provide a consistent experience across multiple systems. At the time of writing, the version in the repositories is v4.2.6. This will not be the latest version, but it should be quite stable, and should be sufficient for quick experimentation with the language.

In order to get this version, we just have to use the apt package manager. We should refresh our local package index first, and then install from the repositories:
```
sudo apt-get update
```
```
sudo apt-get install nodejs
```
This commands install node.js on our server

If the package in the repositories suits your needs, this is all that you need to do to get set up with Node.js. In most cases, you'll also want to also install npm, which is the Node.js package manager. You can do this by typing:
```
sudo apt-get install npm
```
This will allow you to easily install modules and packages to use with Node.js.
You can create a link from node.js to node , it would work just fine:
```
sudo ln -s "$(which nodejs)" /usr/bin/node
```
If you run the above command then you can execute any node file by tying “node” ex: to get version of the node type node-v  . If you didn’t execute this command you have to write nodejs -v, to get version number .

So far we have installed node and change thel ink as well .

Now in the next step we have to install pm2 . Pm2 is a small software which keep the node procress always running in background .

You can get more details about pm2 here (https://www.npmjs.com/package/pm2 )

Note : Any time you make any changes to any code which is in node then you have to restart pm2 process every time to get the latest changes reflected on the code.

To get pm2 procress running you have to type below command .
```
sudo npm install pm2 -g
```
This will install pm2 procress and  -g will install this globally , which mean in any directory in terminal we can type pm2 and it will be accessible .

I will come to pm2 later . 

Now in the next steps lets install mongo database

Run the below commands on terminal to install mongo database on server 
```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
```
We'll create a unit file to manage the MongoDB service. Create a configuration file named mongodb.service in the /etc/systemd/system directory using nano or your favorite text editor.
```
sudo nano /etc/systemd/system/mongodb.service
```

Paste in the following contents
```
 

[Unit]
Description=High-performance, schema-free document-oriented database
After=network.target

[Service]
User=mongodb
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf

[Install]
WantedBy=multi-user.target

```
-----Close this file ----- 
Now run this to start  mongodb
```
sudo systemctl start mongodb
```
While there is no output to this command, you can also use systemctl to check that the service has started properly.
```
sudo systemctl status mongodb
```

Output will run something like this 
```
● mongodb.service - High-performance, schema-free document-oriented database
   Loaded: loaded (/etc/systemd/system/mongodb.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2016-04-25 14:57:20 EDT; 1min 30s ago
 Main PID: 4093 (mongod)
    Tasks: 16 (limit: 512)
   Memory: 47.1M
      CPU: 1.224s
   CGroup: /system.slice/mongodb.service
           └─4093 /usr/bin/mongod --quiet --config /etc/mongod.conf
```

## The last step is to enable automatically starting MongoDB when the system starts.
```
sudo systemctl enable mongodb
```
The MongoDB server now configured and running, and you can manage the MongoDB service using the systemctl command (e.g. sudo systemctl mongodb stop, sudo systemctl mongodb start).

So we have installed Node, Mongodb , Pm2 and npm so far . In the next steps we will install  Node push server .

In order to notify driver and user we have use push messages .  Please check the link https://www.npmjs.com/package/node-pushserver . This has detail explanation of the entire component . This components use to send push message to driver and user all you need to do is just installl this component using below command
```
sudo npm install node-pushserver -g
```
We have installed this using -g which means this node-pushserver is installed globally and hence can be accessed from any folder .

Now we have installed every thing which is required for settting up  .

Lets start  uploading every thing .

Go to  /usr directory from terminal using 
```
Cd /usr
```
Now create a new directory called www by typing
```
Sudo mkdir www
```
Now give 755 permission to that directory using 
```
Sudo chmod 755 www
```
 

Now OPEN FTP 
And copy below folder inside /usr/www

Node-Api folder 
User-Push-Message 
Driver-Push-Message

Once this folder are copied successfully .

Lets  start Node server running .

Back to Terminal -> go to folder 
```
Cd /usr/www/Node-Api folder 
```
Then when you run 
```
node app.js 
```
you get an error 
```
“cannot find module error” . Example it says “cannot find module express “ .
```
In this cases all you need to do is install this module by typing

```
Sudo npm install <module-name>  . Example Sudo npm install express .
```
Keep doing the same till all modules error get resolved once all this are done you will be promoted to successful connection to mongdob .

Now Kill the node ( command -c key ) 
Kill node process by typing  sudo killall -9 node ( This kills all node process. )

Now  in the next steps go back to FTP and open /usr/www/Node-Api/app.js  in your favourite editor .

Once app.js is opened.  Change 2 Things  

Server ip-address which is line number 16. Change this to your ip-address .
Fire-base  key which is on line number 25 . Change this to your firebase key . We use single key between User’s and Driver’s .  ( You can google for how to get firebase key for andriod . We use Firebase for sending push messages for Android .) 
Now Save app.js and run the file again using pm2 
```
Sudo pm2 start app.js --name NodeApi
```

This will start the Node-Server .  To check node-server is started type below ulr on your browser
```
 http://<your-ip-address>:4040 ( This is where your node server is running )
```

It will give some message stating “not able to find index.html” . Now this message is because there is no index.html file . You can create index.html file and enter some message :) But with this your node server has started 


Now lets  connect to mongodb using some GUI tool . In my case we use studio3T (https://studio3t.com/)  . 

In this tool click on Connection -> New Connection -> Enter connection name ( Ex : 24/4 Taxi or any name you prefer ) -> Click on SSHTunnel -> Check on “use ssh tunnel to connect” -> Enter your ssh detail , which you have used to connect to terminal and press  -> Test Connection -> Once tested -> Press Save .

Now on this tool you will see the GUI on the left has all database . Right click and Press “Add Databases”  . Create database called “taxi-booking”  . Now Import the  JSON which we gave inside “Database” -> Mongo-Database-Setup. some steps is duplicated in that document. 

Now after this import one record is created because we want to set up the “loc” column as index 2D . Refer Screenshot for more details .


 




 
Now you have  Node Ready - Mongo Ready with 2D Index . 

Now in the next step we will configure push message 

Let's open FTP Back . If you remember we have copied “User-Push-Message” Folder and “Driver-Push-Message” inside /usr/www .

For For iPhone User app and Driver app . We have to generate .pem file . Now there are lots of tutorial on google which can guide  you to generate .pem file . One of this is below
https://stackoverflow.com/questions/21250510/generate-pem-file-used-to-setup-apple-push-notification

Please  upload this pem file on /usr/www/ for “User-Push-Message” Folder for user and  “Driver-Push-Message”   folder for Driver . Once you have this uploaded you can start the  node push sever with following command

```
Cd /usr/www/User-Push-Message
Sudo pm2 start scriptN.sh --name UserPush
Cd /usr/www/Driver-Push-Message
Sudo pm2 start scriptN.sh --name DriverPush
```
Once this is done

Type this url on browser 
```
<ipaddress>:8001 for users
<ipaddress>:8002 for drivers
```

*If you see some GUI open then you have configured everything correctly .*

With this we installed Node, Mongodb , Push server , Pm2  and started everythign perfectly .



## And also enable mod-rewrite using following 2 commands
```
sudo a2enmod rewrite
sudo service apache2 restart
```

## Also please create htaccess file with below content
```
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
</IfModule>
<IfModule !mod_rewrite.c>
 ErrorDocument 404 index.php
</IfModule>
```
## Rewrite all other URLs to index.php/URL
```
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.php?url=$1 [PT,L] 
</IfModule>
<IfModule !mod_rewrite.c>
 ErrorDocument 404 index.php
</IfModule>
```